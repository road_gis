using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;

using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Timers;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using GIS;
namespace gis_road_main
{
    public class gis_road_basic
    {
        public class road_type_color_value
        {
            public string table_name;
            public string field_name;
            public uint color;
            public double value;
        };
        public class gis_road_detection_unit
        {
            public int road_length;
            public UInt32 ref_road_draw_unit;
            public ArrayList gis_road_detection_type_arr;
            public uint[] road_seg_color; //used to draw the seg;
            //  public double[] road
        };

        public class gis_road_basic_information_unit
        {
            public string road_name;//the road name that get from the db, it should be the same with xml define (map the feature id).
            public int road_feature_id; // the feature read from the xml file.
            public ArrayList archive_date; // the archive date for each road
            public Dictionary<string, road_type_color_value> basic_information_unit_types; // basic road infomation

            public uint road_opening; //color--通车时间
            public uint road_degree;  //color--路面等级
            public uint road_channels; //color--车道数
            public uint road_type; //color--路面类型
            public uint road_suface_struct; //color--路面表层结构
            public PointF[] points;
            public gis_road_detection_unit road_detection_unit;
            public gis_road_basic_information_unit()
            {
                basic_information_unit_types = new Dictionary<string, road_type_color_value>();
            }
            public double[] latitude_longitude;
            public string tip_link_url_pic;
            public string tip_info_string;
            public string tip_link_url;
            public bool is_draw;
        }
        public gis_road_basic_information_unit[] gis_road_basic_information_unit_index = null;
        public bool init_basic_info()
        {
            if (!init_road_name())
                return false;

            return true;
        }
        private void init_preference()
        {
        }
        public bool init_road_name()
        {
            DataSql ds = new DataSql();
            DataTable tb = ds.getRoadName("交管所");
            if (tb == null || tb.Rows.Count == 0)
                return false;

            gis_road_basic_information_unit_index = new gis_road_basic_information_unit[tb.Rows.Count];
            int kk = 0;
            foreach (DataRow dr in tb.Rows)
            {
                string va = Convert.ToString(dr[0]);
                gis_road_basic_information_unit_index[kk] = new gis_road_basic_information_unit();
                gis_road_basic_information_unit_index[kk].road_name = va;
                DataTable tr = ds.getGuiDangRiQi(gis_road_basic_information_unit_index[kk].road_name);
                if (tr != null)
                {
                    gis_road_basic_information_unit_index[kk].archive_date = new ArrayList();
                    foreach (DataRow tdr in tr.Rows)
                    {
                        gis_road_basic_information_unit_index[kk].archive_date.Add(Convert.ToDateTime(tdr[0]));
                    }
                }
                // road_name_featureid.TryGetValue(gis_road_basic_information_unit_index[i].road_name, out fid);
                // gis_road_basic_information_unit_index[kk].road_feature_id = fid;
                kk++;
            }

            return true;
        }
    }
};