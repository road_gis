﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
//using

namespace GIS
{
  public  class DataSql
    {
        public DataSql()
        { }
        public DataTable getJiChuShuJu(string road_name,string table_name,string field_name)
        {
            string strSelect;
            string strFrom;
            string strWhere;
            switch (field_name)
            {
                case "通车时间":
                case "路面等级":
                    strSelect = "distinct " + field_name;
                    break;
                case "上面层材料":
                    strSelect = "起点桩号,止点桩号,上面层材料";
                    break;
                case "当量数":
                    strSelect = "avg(当量数) as "+ field_name;
                    break;
                default:
                    strSelect = "distinct " + field_name;
                    break;
                    

            }
          
            strFrom = "路线信息表 as a, "+table_name+" as b";
            if (field_name != "当量数")
            {
                strWhere = "a.路线名称='" + road_name.ToString() + "' and a.ID=b.路线ID and b.起点桩号>=a.起点桩号 and b.止点桩号<=a.止点桩号";
            }
            else
            {
                strWhere = "a.路线名称='" + road_name.ToString() + "' and a.ID=b.路线ID";
            }
            DataTable dtTemp = GetList(strSelect, strFrom, strWhere).Tables[0];
            if (dtTemp.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return dtTemp;
            }

        }
        public DataTable getGuiDangRiQi(string road_name)
        {
            string strSelect;
            string strFrom;
            string strWhere;        
            strSelect = "distinct b.归档日期";


            strFrom = "路线信息表 as a, guidangxinxi as b";
            strWhere = "a.路线名称='" + road_name.ToString() + "' and a.ID=b.路线";
            DataTable dtTemp = GetList(strSelect, strFrom, strWhere).Tables[0];
            if (dtTemp.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return dtTemp;
            }

        }
      public DataTable getRoadName(string institude)
      {
          return null;
      }
        public DataTable getJianCeShuJu(string road_name, string table_name, string field_name,DateTime RiQi,string LuFu)
        {
            string strSelect;
            string strFrom;
            string strWhere;
            strSelect = "d." + field_name.ToString();
            strFrom = "路线信息表 as a, guidangxinxi as b, fuzhuxinxi as c, "+table_name+" as d";
            strWhere = "a.路线名称='" + road_name.ToString() + "' and a.ID=b.路线 and  b.归档日期='" + RiQi.ToString() + "' and b.ID=c.所属归档 and c.路幅='" + LuFu.ToString() + "' and c.ID=d.To辅助信息ID";
           DataTable  dtTemp= GetList(strSelect, strFrom, strWhere).Tables[0];
            if (dtTemp.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                return dtTemp;
            }

          
        }
      
       private  DataSet GetList(string strSelect, string strFrom, string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select " + strSelect);
            strSql.Append(" FROM "+strFrom);          
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
         
         return GIS.DbHelperSQL.Query(strSql.ToString());
        }

    }
}
