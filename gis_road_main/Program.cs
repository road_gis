﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace gis_road_main
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new form_gis_road());
        }
    }
}